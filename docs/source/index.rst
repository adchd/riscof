#################
Welcome to RISCOF 
#################

.. toctree::
   :glob:
   :maxdepth: 2
   :numbered:

   intro
   overview
   quickstart
   plugins
   testlist
   database
   newtest
   testformat
   RISCV-Config [External] <https://riscv-config.readthedocs.io/>
   code-doc

Indices and tables
-------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
