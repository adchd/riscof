############
Introduction
############


**RISCOF** (RISC-V Compliance Framework) is a YAML based framework which can be used to prove compliance of a RISC-V implementation against the RISC-V privileged and unprivileged ISA spec. 

**Caution**: This is still a work in progress and non-backward compatible changes are expected to happen. 

For more information on the official RISC-V compliance and test suite please visit: `Github <https://github.com/riscv/riscv-compliance>`_

RISCOF [`Repository <https://gitlab.com/incoresemi/riscof>`_]

